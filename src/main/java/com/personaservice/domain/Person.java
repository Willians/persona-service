package com.personaservice.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;

    private String lastNameFather;

    private String lastNameMother;

    private int type;

}
