package com.personaservice.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Client extends Person{
    private UUID idClient;

    private String phone;

    private String email;

    private UUID idUser;
}
