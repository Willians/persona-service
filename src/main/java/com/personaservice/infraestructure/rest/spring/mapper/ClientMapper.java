package com.personaservice.infraestructure.rest.spring.mapper;

import com.personaservice.domain.Client;
import com.personaservice.infraestructure.rest.spring.dto.ClientDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ClientMapper {

    ClientDTO toClientDTO(Client client);

    Client toClientDomain(ClientDTO clientDTO);

    List<ClientDTO> toClientListDTO(List<Client> lstClients);
}
