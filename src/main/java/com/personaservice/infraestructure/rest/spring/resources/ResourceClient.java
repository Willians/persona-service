package com.personaservice.infraestructure.rest.spring.resources;

import com.personaservice.application.service.ClientService;
import com.personaservice.infraestructure.rest.spring.dto.ClientDTO;
import com.personaservice.infraestructure.rest.spring.mapper.ClientMapperImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/client")
public class ResourceClient {

    private final ClientService clientService;

    private final ClientMapperImpl clientMapper;


    @GetMapping("/getAll")
    public ResponseEntity<List<ClientDTO>> getAllClients() {
        List<ClientDTO> lstClientDto = clientMapper.toClientListDTO(clientService.getAllClient());
        if (lstClientDto == null || lstClientDto.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lstClientDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<ClientDTO> getClientById(@PathVariable UUID id) {
        ClientDTO clientDTO = clientMapper.toClientDTO(clientService.getClientById(id));
        if (clientDTO == null) {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(clientDTO, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<ClientDTO> saveClient(@RequestBody ClientDTO clientDTO) {
        return new ResponseEntity<>(clientMapper.toClientDTO(
                clientService.saveClient(
                        clientMapper.toClientDomain(clientDTO)
                )), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ClientDTO> updateClient(@PathVariable UUID id,
                                                  @RequestBody ClientDTO clientDTO) {
        return new ResponseEntity<>(clientMapper.toClientDTO(
                clientService.updateClient(id,
                        clientMapper.toClientDomain(clientDTO))), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Long> deleteClient(@PathVariable UUID id) {
        return new ResponseEntity<>(clientService.deleteClient(id), HttpStatus.NO_CONTENT);
    }
}
