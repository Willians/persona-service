package com.personaservice.infraestructure.rest.spring.resources;

import com.personaservice.application.service.WaiterService;
import com.personaservice.infraestructure.rest.spring.dto.WaiterDTO;
import com.personaservice.infraestructure.rest.spring.mapper.WaiterMapperImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/waiter")
public class ResourceWaiter {

    private final WaiterService waiterService;

    private final WaiterMapperImpl waiterMapper;

    @GetMapping("/getAll")
    public ResponseEntity<List<WaiterDTO>> getAllWaiters(){
        List<WaiterDTO> lstWaiterDto = waiterMapper.toWaiterListDTO(waiterService.getAllWaiter());
        if (lstWaiterDto == null || lstWaiterDto.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        return new ResponseEntity<>(lstWaiterDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<WaiterDTO> getWaiterById(@PathVariable UUID id){
        WaiterDTO waiterDTO = waiterMapper.toWaiterDTO(waiterService.getWaiterById(id));
        if (waiterDTO == null){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(waiterDTO, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<WaiterDTO> saveWaiter(@RequestBody WaiterDTO waiterDTO){
        return new ResponseEntity<>(waiterMapper.toWaiterDTO(waiterService.saveWaiter(waiterMapper.toWaiterDomain(waiterDTO))),HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<WaiterDTO> updateWaiter(@PathVariable UUID id,
                                                  @RequestBody WaiterDTO waiterDTO){
        return new ResponseEntity<>(waiterMapper.toWaiterDTO(waiterService.updateWaiter(id,waiterMapper.toWaiterDomain(waiterDTO))),HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Long> deleteWaiter(@PathVariable UUID id){
        return new ResponseEntity<>(waiterService.deleteWaiter(id), HttpStatus.NO_CONTENT);
    }


}
