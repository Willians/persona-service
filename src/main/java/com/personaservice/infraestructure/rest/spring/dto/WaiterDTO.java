package com.personaservice.infraestructure.rest.spring.dto;

import com.personaservice.domain.Person;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class WaiterDTO extends Person {

    private UUID idWaiter;
}
