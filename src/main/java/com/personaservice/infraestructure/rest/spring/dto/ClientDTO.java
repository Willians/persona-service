package com.personaservice.infraestructure.rest.spring.dto;

import com.personaservice.domain.Person;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO extends Person {
    private UUID idClient;

    private String phone;

    private String email;

    private UUID idUser;
}
