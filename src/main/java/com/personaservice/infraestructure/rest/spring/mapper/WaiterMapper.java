package com.personaservice.infraestructure.rest.spring.mapper;

import com.personaservice.domain.Waiter;
import com.personaservice.infraestructure.rest.spring.dto.WaiterDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface WaiterMapper {

    WaiterDTO toWaiterDTO(Waiter waiter);

    Waiter toWaiterDomain(WaiterDTO waiterDTO);

    List<WaiterDTO> toWaiterListDTO(List<Waiter> lstWaiter);

}
