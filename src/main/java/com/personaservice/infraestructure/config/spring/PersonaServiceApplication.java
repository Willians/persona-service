package com.personaservice.infraestructure.config.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.personaservice.infraestructure")
@EntityScan(basePackages = "com.personaservice.domain")
public class PersonaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonaServiceApplication.class, args);
	}

}
