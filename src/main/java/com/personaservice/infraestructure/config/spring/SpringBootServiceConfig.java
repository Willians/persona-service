package com.personaservice.infraestructure.config.spring;

import com.personaservice.application.repository.ClientRepository;
import com.personaservice.application.repository.WaiterRepository;
import com.personaservice.application.service.ClientService;
import com.personaservice.application.service.WaiterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringBootServiceConfig {

    @Bean
    public WaiterService waiterService(WaiterRepository waiterRepository){
        return new WaiterService(waiterRepository);
    }

    @Bean
    public ClientService clientService(ClientRepository clientRepository){
        return new ClientService(clientRepository);
    }
}
