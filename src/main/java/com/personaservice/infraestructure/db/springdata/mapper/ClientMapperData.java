package com.personaservice.infraestructure.db.springdata.mapper;

import com.personaservice.domain.Client;
import com.personaservice.infraestructure.db.springdata.dbo.ClientEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientMapperData {

    Client toDomain(ClientEntity clientEntity);

    @InheritInverseConfiguration
    ClientEntity toEntity(Client client);


    List<Client> toDomainList(List<ClientEntity> lsClientEntities);

}
