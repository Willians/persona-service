package com.personaservice.infraestructure.db.springdata.repository;

import com.personaservice.application.repository.ClientRepository;
import com.personaservice.domain.Client;
import com.personaservice.infraestructure.db.springdata.dbo.ClientEntity;
import com.personaservice.infraestructure.db.springdata.mapper.ClientMapperDataImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class ClientDboRepository implements ClientRepository {

    private final ClientMapperDataImpl clientMapperData;

    private final SpringDataClientRepository springDataClientRepository;

    @Override
    public Client findById(UUID id) {
        if (springDataClientRepository.findById(id).isEmpty()){
            return null;
        }
        return clientMapperData.toDomain(springDataClientRepository.findById(id).get());
    }

    @Override
    public Client save(Client client) {
        return clientMapperData.toDomain(springDataClientRepository.save(clientMapperData.toEntity(client)));
    }

    @Override
    public Client update(UUID id, Client client) {
        Client client1 = findById(id);
        if (client1 == null){
            try {
                throw new Exception("No existe el Cliente con id "+id);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        client.setIdClient(id);
        ClientEntity clientEntity = clientMapperData.toEntity(client);
        return save(clientMapperData.toDomain(clientEntity));
    }

    @Override
    public List<Client> getAll() {
        return clientMapperData.toDomainList(springDataClientRepository.findAll());
    }

    @Transactional
    @Override
    public long deleteById(UUID id) {
        return springDataClientRepository.deleteByIdClient(id);
    }
}
