package com.personaservice.infraestructure.db.springdata.repository;

import com.personaservice.infraestructure.db.springdata.dbo.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SpringDataClientRepository extends JpaRepository<ClientEntity, UUID> {

    long deleteByIdClient(UUID id);
}
