package com.personaservice.infraestructure.db.springdata.repository;

import com.personaservice.application.repository.WaiterRepository;
import com.personaservice.domain.Waiter;
import com.personaservice.infraestructure.db.springdata.dbo.WaiterEntity;
import com.personaservice.infraestructure.db.springdata.mapper.WaiterMapperDataImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class WaiterDboRepository implements WaiterRepository {

    private final WaiterMapperDataImpl waiterMapperData;

    private final SpringDataWaiterRepository springDataWaiterRepository;

    @Override
    public Waiter findById(UUID id) {
        if (springDataWaiterRepository.findById(id).isEmpty()){
            return null;
        }
        return waiterMapperData.toDomain(springDataWaiterRepository.findById(id).get());
    }

    @Override
    public Waiter save(Waiter waiter) {
        return waiterMapperData.toDomain(springDataWaiterRepository.save(waiterMapperData.toEntity(waiter)));
    }

    @Override
    public Waiter update(UUID id, Waiter waiter) {
        Waiter waiter1 = findById(id);
        if (waiter1 == null){
            try {
                throw new Exception("No existe el camarero con id "+id);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        waiter.setIdWaiter(id);
        WaiterEntity waiterEntity = waiterMapperData.toEntity(waiter);
        return save(waiterMapperData.toDomain(waiterEntity));
    }

    @Override
    public List<Waiter> getAll() {
        return waiterMapperData.toDomainList(springDataWaiterRepository.findAll());
    }

    @Transactional
    @Override
    public long deleteById(UUID id) {
        return springDataWaiterRepository.deleteByIdWaiter(id);
    }
}
