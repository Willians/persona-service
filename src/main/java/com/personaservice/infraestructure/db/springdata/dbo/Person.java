package com.personaservice.infraestructure.db.springdata.dbo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Person {

    @Column
    private String name;

    @Column
    private String lastNameFather;

    @Column
    private String lastNameMother;

    @Column
    private int type;


}
