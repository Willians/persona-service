package com.personaservice.infraestructure.db.springdata.mapper;

import com.personaservice.domain.Waiter;
import com.personaservice.infraestructure.db.springdata.dbo.WaiterEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WaiterMapperData {

    Waiter toDomain(WaiterEntity waiterEntity);

    @InheritInverseConfiguration
    WaiterEntity toEntity(Waiter waiter);

    List<Waiter> toDomainList(List<WaiterEntity> lstWaiterEntities);

}
