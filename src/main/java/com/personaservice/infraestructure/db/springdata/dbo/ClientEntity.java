package com.personaservice.infraestructure.db.springdata.dbo;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "client")
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ClientEntity{

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "idCliente")
    private UUID idClient;

    @Column(name = "nombre")
    private String name;

    @Column(name = "apellidoPaterno")
    private String lastNameFather;

    @Column(name = "apellidoMaterno")
    private String lastNameMother;

    @Column(name = "tipo", length = 1)
    private int type;

    @Column(name = "telefono")
    private String phone;

    @Column(name = "correo")
    private String email;

    @Column(name = "idUsuario")
    private UUID idUser;

}
