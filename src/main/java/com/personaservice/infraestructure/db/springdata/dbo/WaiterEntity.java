package com.personaservice.infraestructure.db.springdata.dbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "waiter")
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class WaiterEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "idCamarero")
    private UUID idWaiter;

    @Column(name = "nombre")
    private String name;

    @Column(name = "apellidoPaterno")
    private String lastNameFather;

    @Column(name = "apellidoMaterno")
    private String lastNameMother;

    @Column(name = "tipo", length = 1)
    private int type;

}
