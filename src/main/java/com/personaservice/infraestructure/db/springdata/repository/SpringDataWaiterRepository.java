package com.personaservice.infraestructure.db.springdata.repository;

import com.personaservice.infraestructure.db.springdata.dbo.WaiterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SpringDataWaiterRepository extends JpaRepository<WaiterEntity, UUID> {

    long deleteByIdWaiter(UUID id);
}
