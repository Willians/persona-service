package com.personaservice.application.repository;

import com.personaservice.domain.Client;

import java.util.List;
import java.util.UUID;

public interface ClientRepository {

    Client findById(UUID id);

    Client save(Client client);

    Client update(UUID id, Client client);

    List<Client> getAll();

    long deleteById(UUID id);
}
