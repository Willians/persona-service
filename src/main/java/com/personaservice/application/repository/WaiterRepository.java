package com.personaservice.application.repository;

import com.personaservice.domain.Waiter;

import java.util.List;
import java.util.UUID;

public interface WaiterRepository {

    Waiter findById(UUID id);

    Waiter save(Waiter waiter);

    Waiter update(UUID id, Waiter waiter);

    List<Waiter> getAll();

    long deleteById(UUID id);
}
