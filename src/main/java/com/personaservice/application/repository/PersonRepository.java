package com.personaservice.application.repository;

import com.personaservice.domain.Person;

import java.util.UUID;

public interface PersonRepository {

    Person findById(UUID id);

}
