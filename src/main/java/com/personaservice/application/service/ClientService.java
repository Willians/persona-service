package com.personaservice.application.service;

import com.personaservice.application.repository.ClientRepository;
import com.personaservice.domain.Client;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class ClientService {

    private final ClientRepository clientRepository;

    public Client getClientById(UUID id){
        return clientRepository.findById(id);
    }

    public Client saveClient(Client client){
        return clientRepository.save(client);
    }

    public Client updateClient(UUID id, Client client){
        return clientRepository.update(id, client);
    }

    public List<Client> getAllClient(){
        return clientRepository.getAll();
    }

    public long deleteClient(UUID id){
        return clientRepository.deleteById(id);
    }
}
