package com.personaservice.application.service;

import com.personaservice.application.repository.WaiterRepository;
import com.personaservice.domain.Waiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class WaiterService {

    private final WaiterRepository waiterRepository;

    public Waiter getWaiterById(UUID id){
        return waiterRepository.findById(id);
    }

    public Waiter saveWaiter(Waiter waiter){
        return waiterRepository.save(waiter);
    }

    public Waiter updateWaiter(UUID id, Waiter waiter){
        return waiterRepository.update(id, waiter);
    }

    public List<Waiter> getAllWaiter(){
        return waiterRepository.getAll();
    }

    public long deleteWaiter(UUID id){
        return waiterRepository.deleteById(id);
    }
}
