package com.personaservice.application.service;

import com.personaservice.application.repository.PersonRepository;
import com.personaservice.domain.Person;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
public class PersonService {

    private final PersonRepository personRepository;

    public Person getPerson(UUID id){
        return personRepository.findById(id);
    }
}
